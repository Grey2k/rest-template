import * as jwt from "jsonwebtoken";
import { JWT_TTL, PRIVATE_KEY } from "../../config/jwt/keys";

export class JwtGenerator {
    public static generateToken() {
        let payload = {
            jti: "8c6ecfdc989a62431fb1050e33c64b5d",
            iss: "auth.beget.com",
            exp: Math.floor(Date.now() / 1000) + JWT_TTL,
            sub: "customer",
            ip: "127.0.0.1",
            customerId: 37849,
            customerLogin: "grey2k",
        };

        let token = jwt.sign(payload, PRIVATE_KEY, {
            algorithm: "RS512",
        });

        return token;
    }
}
