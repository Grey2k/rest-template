import * as assert from "assert";
import * as fetch from "isomorphic-fetch";

// describe('webdriver.io page',  () => {
//     it('should have the right title - the fancy generator way', (done) => {
//         browser.url('http://webdriver.io');
//         return assert.equal(browser.getTitle(), 'WebdriverIO - Selenium 2.0 javascript bindings for nodejs');
//
//     });
// });

const jwtPublicKey = `-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtFHZdeQFUb+WJUZQtlC8
065/pwGNNt60c6KbZZCIMhyoP2bqMqgkFWf6qIh/+KvJgOCTZ2TzEnxHnO0FZNPL
oMyeX3TgqXGQjHCltM6QLC2PtRW1Mot8eKM3NGxLvWM3ABIl1wK3Ae/kjUz1aJTI
QXgF4kDhh5NN3Vq3JdkO1/7MzK6YIUyyQadooKrnsn6nSkYTWjqG2PUO/0lld+p5
48kOZ65xwrXoDLy/n3qRs4hm44hLB5VgaTuzVirL5DfcVWei/wnynJtOsnkhZzL3
eNOJHyi1RcXAkAgKyfwNvi3is4/ASIEe4ochBHus/YxhUYxmTAWbSgxFskjxWK6F
9wIDAQAB
-----END PUBLIC KEY-----
`;

describe("[GET] /key", () => {
    it("it should GET key", () => {
        let value = "";

        browser.call(() =>
            new Promise((resolve, reject) => {
                fetch("http://sgoppikov.dev.beget:9180/key")
                    .then((res) => {
                        return res.json();
                    })
                    .then((json) => {
                        value = json.key;
                        resolve(json.key);
                    })
                    .catch((e) => {
                        reject(e);
                    });
            }));

        assert.equal(value, jwtPublicKey);
    });
});
