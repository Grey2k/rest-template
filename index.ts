import express = require("express");
import bodyParser = require("body-parser");
import _ = require("lodash");
import cors = require("cors");
import { JwtGenerator } from "./src/services/jwt";

import Request = express.Request;
import Response = express.Response;

let app = express();

// CORS middleware
app.use(cors({
    origin: "*",
}));

// parse application/json
app.use(bodyParser.json());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true,
}));

// routes
app.get("/key", (req: Request, res: Response) => {
    let result = {
        key: "",
    };

    res.json(result);
});

// routes
app.post("/auth", (req: Request, res: Response) => {

    res.status(201).json({
        token: JwtGenerator.generateToken(),
    });
});

app.all(/.*/, (req: Request, res: Response) => {
    res.status(404).send();
});

app.listen(3000, () => {
    // tslint:disable-next-line
    console.log("Example app listening on port 3000!");
});
